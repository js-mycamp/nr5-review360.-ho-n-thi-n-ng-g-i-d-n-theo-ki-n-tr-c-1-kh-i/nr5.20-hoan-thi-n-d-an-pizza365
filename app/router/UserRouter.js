//Khai báo thư viện express
const express = require("express");

//Khai báo Middleware 
const {
    getAllUserMiddleware,
    getAUserMiddleware,
    putUserMiddleware,
    postUserMiddleware,
    deleteUserMiddleware,
    getUserLimitMiddleware,
    getUserSkipMiddleware,
    getUserSortMiddleware,
    getUserLimitSkipMiddleware,
    getUserSortLimitSkipMiddleware
} = require(`../middleware/UserMiddleware`);

// khai báo Usercontroller 
const {
    creatUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
    getUserLimit,
    getUserSkip,
    getAllUserSorted,
    getUserLimitSkip,
    getUserSortLimitSkip
} = require("../controller/UserController")
//Tạo rouer 
const UserRouter = express.Router();

//Sử dụng router
UserRouter.get("/User",getAllUserMiddleware,getAllUser);

UserRouter.get("/User/:UserId",getAUserMiddleware,getUserById);

UserRouter.post("/User",postUserMiddleware,creatUser);

UserRouter.put("/User/:UserId",putUserMiddleware,updateUserById);

UserRouter.delete("/User/:UserId",deleteUserMiddleware,deleteUserById);

UserRouter.get("/limit-users",getUserLimitMiddleware,getUserLimit);

UserRouter.get("/skip-users",getUserSkipMiddleware,getUserSkip);

UserRouter.get("/sort-users", getUserSortMiddleware,getAllUserSorted);

UserRouter.get("/skip-limit-users",getUserLimitSkipMiddleware,getUserLimitSkip);

UserRouter.get("/sort-skip-limit-users",getUserSortLimitSkipMiddleware,getUserSortLimitSkip)

module.exports = {UserRouter};
